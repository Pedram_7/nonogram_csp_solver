import itertools
from nonogram_csp import begin_solve

board_size = 0

row_size, col_size = 0, 0


class Variable:
    name = ""
    specs = []
    domain = []
    is_filled = False
    fill_value = 0

    def init_domain(self):
        if (sum(self.specs) + len(self.specs) - 1 > col_size and self.name[0] == 'R') or (
                sum(self.specs) + len(self.specs) - 1 > row_size and self.name[0] == 'C'):
            print("what the bloody hell")
        else:
            if self.name[0] == 'R':
                self.domain = generate_domains_from_specifications(col_size, self.specs)
            else:
                self.domain = generate_domains_from_specifications(row_size, self.specs)

    def __init__(self, name, amounts):
        self.specs = amounts
        self.name = name
        self.init_domain()


def generate_domains_from_specifications(length, specifications):
    # Clever function to generate possible domains for a rows and cols
    domain = []
    min_placement = []
    for s in specifications:
        for i in range(s):
            min_placement.append(1)
        min_placement.append(0)
    min_placement.pop(len(min_placement) - 1)

    insert_indices = [i + 1 for i, x in enumerate(min_placement) if x == 0]
    insert_indices.extend([0, len(min_placement)])
    combinations = itertools.combinations_with_replacement(insert_indices, length - len(min_placement))

    for c in combinations:
        result = min_placement[:]
        insert_positions = list(c)
        insert_positions.sort()
        offset = 0
        for index in insert_positions:
            result.insert(index + offset, 0)
            offset += 1
        domain.append(result)
    return domain


def init_csp(rows, cols):
    row_variables = []
    col_variables = []
    for i in range(row_size):
        var = Variable('R' + str(i), rows[i])
        row_variables.append(var)
    for i in range(col_size):
        var = Variable('C' + str(i), cols[i])
        col_variables.append(var)
    variables = row_variables + col_variables
    return variables


def read_input():
    global board_size
    board_size = int(input())
    rows = [[]] * board_size
    cols = [[]] * board_size
    for i in range(board_size):
        rows[i] = [int(item) for item in input().split()]
    for i in range(board_size):
        cols[i] = [int(item) for item in input().split()]
    print(rows)
    print(cols)


def read_file(address):
    with open(address, 'r') as f:
        fr = f.readlines()
        global row_size, col_size
        arr = fr[0].split(' ')
        row_size = int(arr[0])
        if len(arr) > 1:
            col_size = int(arr[1])
        else:
            col_size = row_size

        rows = [[]] * row_size
        cols = [[]] * col_size
        for i in range(row_size):
            rows[i] = [int(item) for item in fr[i + 1].split()]
            rows[i].remove(rows[i][0])
        for i in range(col_size):
            cols[i] = [int(item) for item in fr[i + row_size + 1].split()]
            cols[i].remove(cols[i][0])
    init_vars = init_csp(rows, cols)
    print(str(row_size) + ',' + str(col_size))
    print(rows)
    print(cols)

    begin_solve(init_vars)


if __name__ == '__main__':
    read_file('input4.txt')
