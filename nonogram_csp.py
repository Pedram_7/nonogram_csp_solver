import copy

answer_path = []
permutations = 1
ct = 0


def ctp():
    global ct
    ct += 1


def getct():
    global ct
    return ct


def begin_solve(init_variables):
    global permutations
    rows = list(filter(lambda v: 'R' in v.name, init_variables))
    cols = list(filter(lambda v: 'C' in v.name, init_variables))
    trash = 1

    while trash > 0:
        trash = 0
        for r in rows:
            t, cols = propagate_row_to_cols(r, cols)
            trash += t
        for c in cols:
            trash2, rows = propagate_col_to_rows(c, rows)
            trash += trash2

    for r in rows:
        permutations *= len(r.domain)
    print('permutations = ' + str(permutations))
    solve(rows, cols)


def domain_length(var):
    return len(var.domain)


def contains_var_with_empty_domain(variables):
    for var in variables:
        if len(var.domain) == 0:
            return True
    return False


def print_answer():
    rows = list(filter(lambda v: 'R' in v[0].name, answer_path))
    rows.sort(key=lambda x: int(x[0].name[1:]))
    rows = [(x[1]) for x in rows]
    for row in rows:
        print((''.join(str(x) for x in row)).replace('1', '*').replace('0', '.'))


def solve(rows, cols):
    if getct() % 1000 == 0:
        print("dep , ct :  " + str(len(answer_path)) + '  ,  ' + str(getct()))
    ctp()
    ### Selecting Variable with minimum branch factor
    if len(rows) == 0:
        print_answer()
        exit(0)
    # rows = list(filter(lambda v: 'R' in v.name, vars))
    # cols = list(filter(lambda v: 'C' in v.name, vars))
    sorted_rows = sorted(rows, key=domain_length)

    for i, var in enumerate(sorted_rows):
        children_tuples = []
        for value in var.domain:
            ct, new_rows, new_cols, row_value = (constraint_propagate(i, value, sorted_rows, cols))
            if contains_var_with_empty_domain(new_cols) or contains_var_with_empty_domain(new_rows):
                continue
            else:
                temp = (ct, new_rows, new_cols, row_value)
                children_tuples.append(temp)
        children_tuples.sort(key=lambda x: x[0])
        for child in children_tuples:
            t1 = (var, child[3])
            answer_path.append(t1)
            solve(child[1], child[2])
            answer_path.remove(t1)

    ### Selecting Least Constraining value


def has_no_contradiction(row_number, row_value, col_number, col_value):
    return row_value[col_number] == col_value[row_number]


def row_domain_has_no_contradiction_with_col(row_number, row_value, col):
    col_num = int(col.name[1:])
    for dom_val in col.domain:
        if has_no_contradiction(row_number, row_value, col_num, dom_val):
            return True
    return False


def propagate_row_to_cols(row, cols):
    cnt = 0
    for col in cols:
        col_num = int(col.name[1:])
        l1 = len(col.domain)
        col.domain = list(filter(lambda x: row_domain_has_no_contradiction_with_col(col_num, x, row), col.domain))
        cnt += abs(l1 - len(col.domain))
    return cnt, cols


def propagate_col_to_rows(col, rows):
    cnt = 0

    for row in rows:
        row_num = int(row.name[1:])
        l1 = len(row.domain)
        row.domain = list(filter(lambda x: row_domain_has_no_contradiction_with_col(row_num, x, col), row.domain))
        cnt += abs(l1 - len(row.domain))
    return cnt, rows


def constraint_propagate(row_index, row_value, row_variables, col_variables):
    new_cols = copy.deepcopy(col_variables)
    new_rows = copy.deepcopy(row_variables)
    count = 0
    for var in new_cols:
        len1 = len(var.domain)
        var.domain = list(filter(
            lambda x: has_no_contradiction(int(row_variables[row_index].name[1:]), row_value, int(var.name[1:]), x),
            var.domain))
        count += abs(len(var.domain) - len1)
    new_rows.pop(row_index)
    count2 = 0
    for col in col_variables:
        temp, new_rows = propagate_col_to_rows(col, new_rows)
        count2 += temp

    trash = 1
    while trash > 0:
        trash = 0
        for r in new_rows:
            t, new_cols = propagate_row_to_cols(r, new_cols)
            trash += t
        for c in new_cols:
            trash2, new_rows = propagate_col_to_rows(c, new_rows)
            trash += trash2

    return count2 + count, new_rows, new_cols, row_value
